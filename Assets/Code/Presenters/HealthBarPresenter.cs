﻿using Assets.Code.Models;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.Presenters
{
    public class HealthBarPresenter : MonoBehaviour
    {
        [SerializeField]
        private Text _text;

        [SerializeField]
        private Image _fill;

        [SerializeField]
        private Transform _player;

        private float _oldHp;
        public void Init(PlayerModel playerModel)
        {
            var barYPosition = 3.5f;
            Observable.EveryUpdate()
                .Subscribe(l => transform.position = Camera.main.WorldToScreenPoint(new Vector3(_player.position.x, _player.position.y + barYPosition, _player.position.z)))
                .AddTo(gameObject);

            playerModel.Hp.Select(hp => hp > 0)
                .Subscribe(isAlive => gameObject.SetActive(isAlive))
                .AddTo(this);

            playerModel.Hp.Subscribe(newHp =>
            {
                var oldAmount = _fill.fillAmount;
                var newAmount = newHp / playerModel.StatList.MaxHealth.Value;
                _fill.fillAmount = newAmount;

                DoTextAnimation(oldAmount, newAmount, newHp);
            }).AddTo(this);
        }

        private void DoTextAnimation(float oldAmount, float newAmount, float newHpValue)
        {
            var ngo = Instantiate(_text, transform, true);

            ngo.text = $"{newHpValue - _oldHp:+0.#;-0.#}";

            ngo.color = newAmount > oldAmount ? Color.green : Color.red;

            var startPos = ngo.transform.localPosition.y;
            var startTime = Time.time;

            const float animationTime = 1.5f;
            const float upToHeight = 3f;

            _oldHp = newHpValue;

            Observable.EveryUpdate()
                .Select(time => Time.time - startTime)
                .TakeWhile(elapsedSec => elapsedSec <= animationTime)
                .Subscribe(l => ngo.transform.Translate(0, Mathf.Lerp(startPos, startPos + upToHeight, l / animationTime), 0), () => Destroy(ngo.gameObject));
        }
    }
}