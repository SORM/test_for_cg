﻿using Assets.Code.Models;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Code.Presenters
{
    public class GameProcPresenter : MonoBehaviour, IInitializable
    {
        [SerializeField]
        private Button _playButton;
        [SerializeField]
        private Button _playBuffsButton;

        private GameProcModel _gameProcModel;

        [Inject]
        private void Init(GameProcModel gameProcModel)
        {
            _gameProcModel = gameProcModel;
        }
        
        public void Initialize()
        {
            _gameProcModel.Play.BindTo(_playButton);
            _gameProcModel.PlayWithBuffs.BindTo(_playBuffsButton);
        }
    }
}