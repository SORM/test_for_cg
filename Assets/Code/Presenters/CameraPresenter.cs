﻿using System;
using Assets.Code.Services;
using UnityEngine;
using Zenject;
using UniRx;
namespace Assets.Code.Presenters
{
    public class CameraPresenter : IDisposable
    {
        private ISettingService _settingService;
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        private void Init(ISettingService settingService)
        {
            var mainCamera = Camera.main;

            _settingService = settingService;

            _settingService.SettingsAsColdObservable.Subscribe(data =>
            {
                var cs = data.cameraSettings;

                mainCamera.transform.position = new Vector3(-cs.roundRadius, cs.height, 0);
                mainCamera.transform.LookAt(new Vector3(0, cs.lookAtHeight, 0));

                Observable.EveryUpdate()
                    .Subscribe(l => mainCamera.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * cs.roundDuration))
                    .AddTo(_disposable);

                var zoomIntervalSec = cs.roundDuration / 2;

                // every zoomIntervalSec 
                Observable.Interval(TimeSpan.FromSeconds(zoomIntervalSec)).Subscribe(l =>
                {
                    var startTime = Time.time;
                    // start zoom from min to max in cs.fovDuration:
                    Observable.EveryUpdate()
                        .Select(time => Time.time - startTime)
                        .TakeWhile(elapsedSec => elapsedSec <= cs.fovDuration)
                        .Select(elapsedSec => elapsedSec / cs.fovDuration)
                        .Subscribe(lk => mainCamera.fieldOfView = Mathf.Lerp(cs.fovMax, cs.fovMin, lk));

                    // wait zoom and delay time:
                    Observable.Timer(TimeSpan.FromSeconds(cs.fovDelay + cs.fovDuration)).Subscribe(l1 =>
                    {
                        var waitStartTime = Time.time;
                        Observable.EveryUpdate()
                            .Select(time => Time.time - waitStartTime)
                            .TakeWhile(elapsedSec => elapsedSec <= cs.fovDuration)
                            .Select(elapsedSec => elapsedSec / cs.fovDuration)
                            .Subscribe(lk => mainCamera.fieldOfView = Mathf.Lerp(cs.fovMin, cs.fovMax, lk));
                    });
                }).AddTo(_disposable);

            }).AddTo(_disposable);
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}