﻿using Assets.Code.Models;
using Assets.Code.Services;
using Zenject;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Code.Presenters
{
    public class StatPresenter : MonoBehaviour
    {
        [SerializeField]
        private Text _text;

        [SerializeField]
        private Image _icon;

        [Inject]
        private void Init(StatModel statModel, ISpriteCache spriteCache)
        {
            statModel.TextValue.SubscribeToText(_text).AddTo(this);
            statModel.IconName.Subscribe(iconName =>
            {
                var sprite = spriteCache.Get(iconName);
                _icon.sprite = sprite;
            });
        }

        public class StatPresenterFactory : Factory<StatModel, StatPresenter> { }
    }
}