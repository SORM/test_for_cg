﻿using Assets.Code.Models;
using Assets.Code.Services;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Code.Presenters
{
    public class BuffPresenter : MonoBehaviour
    {
        [SerializeField]
        private Text _text;

        [SerializeField]
        private Image _icon;

        [Inject]
        private void Init(BuffModel buffModel, ISpriteCache spriteCache)
        {
            buffModel.TextValue.SubscribeToText(_text).AddTo(this);
            buffModel.IconName.Subscribe(iconName =>
            {
                var sprite = spriteCache.Get(iconName);
                _icon.sprite = sprite;
            });
        }

        public class BuffPresenterFactory : Factory<BuffModel, BuffPresenter>{}
    }
}