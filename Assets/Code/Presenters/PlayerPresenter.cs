﻿using System.Collections.Generic;
using Assets.Code.Models;
using UniRx;
using UnityEngine;
using Zenject;

namespace Assets.Code.Presenters
{
    public class PlayerPresenter : PlayerPanelHierarchy
    {
        [SerializeField]
        private HealthBarPresenter _healthBarPresenter;

        private IEnumerable<StatPresenter> CurrentStatPresenters => statsPanel.GetComponentsInChildren<StatPresenter>();
        private IEnumerable<BuffPresenter> CurrentBuffPresenters => statsPanel.GetComponentsInChildren<BuffPresenter>();

        private StatModel _lifeModel;

        [Inject]
        private void Init(PlayerModel playerModel, StatPresenter.StatPresenterFactory statPresenterFactory, BuffPresenter.BuffPresenterFactory buffPresenterFactory)
        {            
            _healthBarPresenter.Init(playerModel);

            attackButton.onClick.AsObservable().Subscribe(unit =>
            {
                if (playerModel.Attack.Execute())
                    character.SetTrigger("Attack");
            });

            playerModel.Hp.Subscribe(hp =>
            {
                character.SetInteger("Health", (int)hp);
                if (_lifeModel != null)
                    _lifeModel.Value.Value = hp;
            });

            playerModel.StatList.StatsCollection.ObserveReset().Subscribe(unit =>
            {
                foreach (var statPresenter in CurrentStatPresenters) DestroyImmediate(statPresenter.gameObject);
                foreach (var buffPresenter in CurrentBuffPresenters) DestroyImmediate(buffPresenter.gameObject);
            });

            playerModel.StatList.StatsCollection.ObserveAdd().Subscribe(eventData =>
            {
                var newModel = eventData.Value;
                var ps = statPresenterFactory.Create(newModel);
                ps.transform.SetParent(statsPanel, false);                
            });

            playerModel.StatList.BuffCollection.ObserveAdd().Subscribe(eventData =>
            {
                var newModel = eventData.Value;
                var ps = buffPresenterFactory.Create(newModel);
                ps.transform.SetParent(statsPanel,false);
            });

            playerModel.StatList.StatsCollection.ObserveAdd()
                .Where(eventData => eventData.Value.Id == StatsId.LIFE_ID)
                .Select(eventData => eventData.Value)
                .Subscribe(lifeModel => { _lifeModel = lifeModel; });
        }
    }
}