﻿using UniRx;

namespace Assets.Code.Models
{
    public class StatModel
    {
        public Stat Stat { get; private set; }
        public int Id => Stat.id;

        public readonly ReactiveProperty<float> Value = new ReactiveProperty<float>();
        public readonly IReactiveProperty<string> IconName = new ReactiveProperty<string>();

        public IReadOnlyReactiveProperty<string> TextValue { get; }

        public StatModel()
        {
            TextValue = Value.Select(v => $"{v:0.#}").ToReadOnlyReactiveProperty(string.Empty);
        }
        
        public StatModel Init(Stat stat)
        {
            Stat = stat;
            Value.Value = stat.value;
            ((ReactiveProperty<string>)IconName).Value = stat.icon;
            return this;
        }
    }
}