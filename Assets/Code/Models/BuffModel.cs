﻿using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace Assets.Code.Models
{
    public class BuffModel
    {
        public Buff Buff { get; private set; }
        private Dictionary<int, float> _buffStatValues;

        public readonly IReadOnlyReactiveProperty<string> IconName = new ReactiveProperty<string>();
        public readonly IReadOnlyReactiveProperty<string> TextValue = new ReactiveProperty<string>();
        
        public BuffModel Init(Buff buff)
        {
            Buff = buff;
            _buffStatValues = Buff.stats.ToDictionary(bs => bs.statId, bs => bs.value);
            ((ReactiveProperty<string>)IconName).Value = Buff.icon;
            ((ReactiveProperty<string>)TextValue).Value = Buff.title;
            return this;
        }
        
        public virtual void ApplyTo(IEnumerable<StatModel> allStatModels)
        {
            foreach (var statModel in allStatModels.Where(s => _buffStatValues.Keys.Contains(s.Id)))
            {
                statModel.Value.Value += _buffStatValues[statModel.Id];
            }
        }
    }
}