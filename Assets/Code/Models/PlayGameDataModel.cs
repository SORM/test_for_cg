﻿namespace Assets.Code.Models
{
    public class PlayGameDataModel
    {
        public StatModel[] StatModels { get; }
        public BuffModel[] BuffModels { get; }
        public int BuffsMax { get; }
        public int BuffsMin { get; }
        public bool BuffwAllowDublicates { get; }

        public PlayGameDataModel(StatModel[] statModels, BuffModel[] buffModels, int buffsMax, int buffsMin, bool buffwAllowDublicates)
        {
            StatModels = statModels;
            BuffModels = buffModels;
            BuffsMax = buffsMax;
            BuffsMin = buffsMin;
            BuffwAllowDublicates = buffwAllowDublicates;
        }

    }
}