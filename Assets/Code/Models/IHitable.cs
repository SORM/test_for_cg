﻿using UniRx;

namespace Assets.Code.Models
{
    public interface IHitable
    {
        IReadOnlyReactiveProperty<float> Hp { get; }
        float Hit(float damage);
    }
}