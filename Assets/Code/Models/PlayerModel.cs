﻿using System;
using System.Linq;
using UniRx;

namespace Assets.Code.Models
{
    public class PlayerModel : IHitable
    {
        public IReadOnlyReactiveProperty<float> Hp => _hp;
        public ReactiveProperty<IHitable> Enemy { get; } = new ReactiveProperty<IHitable>();

        public readonly ReactiveCommand Attack;
        public readonly ReactiveCommand<PlayGameDataModel> Resurect;

        private readonly ReactiveProperty<float> _hp = new ReactiveProperty<float>(100);
        public readonly StatList StatList = new StatList();

        public PlayerModel()
        {
            Attack = Hp.Select(newHp => newHp > 0)
                .Merge(Enemy.Select(e => e != null))
                .ToReactiveCommand();

            Attack.Where(unit => Enemy.Value != null).Subscribe(enemy =>
            {
                var damage = StatList.StatsCollection.First(s => s.Id == StatsId.DAMAGE_ID);
                var totalDamage = Enemy.Value.Hit(damage.Value.Value);

                var vamp = StatList.StatsCollection.First(b => b.Id == StatsId.LIFE_STEAL_ID);
                if (vamp.Value.Value > 0)
                {
                    var totalAdd = totalDamage * Math.Min(1, vamp.Value.Value / 100f);
                    var hpValue = Math.Min(totalAdd + _hp.Value, StatList.MaxHealth.Value);
                    _hp.Value = hpValue;
                }
            });

            Resurect = new ReactiveCommand<PlayGameDataModel>();
            Resurect.Subscribe(gameDataModel =>
            {
                StatList.Init(gameDataModel.StatModels, gameDataModel.BuffModels);

                _hp.Value = StatList.MaxHealth.Value;
            });
        }

        float IHitable.Hit(float damage)
        {
            var armor = StatList.StatsCollection.First(s => s.Id == StatsId.ARMOR_ID);
            var totalDamage = damage * (1 - Math.Min(1, armor.Value.Value / 100));
            _hp.Value = Math.Max(0, _hp.Value - totalDamage);
            return totalDamage;
        }
    }
}