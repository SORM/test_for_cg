﻿using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace Assets.Code.Models
{
    public class StatList
    {
        public ReactiveCollection<StatModel> StatsCollection { get; } = new ReactiveCollection<StatModel>(new List<StatModel>());
        public ReactiveCollection<BuffModel> BuffCollection { get; } = new ReactiveCollection<BuffModel>(new List<BuffModel>());
        public IReadOnlyReactiveProperty<float> MaxHealth { get; } = new ReactiveProperty<float>();
        
        public void Clear()
        {
            StatsCollection.Clear();
            BuffCollection.Clear();
        }

        public void Init(IEnumerable<StatModel> statModels, IEnumerable<BuffModel> buffModels)
        {
            Clear();
            foreach (var statModel in statModels) StatsCollection.Add(statModel);
            foreach (var buffModel in buffModels) BuffCollection.Add(buffModel);
            ((ReactiveProperty<float>)MaxHealth).Value = StatsCollection.First(s => s.Id == StatsId.LIFE_ID).Value.Value;
        }

        public StatList()
        {
            BuffCollection.ObserveAdd().Subscribe(eventData =>
            {
                var buffModel = eventData.Value;
                buffModel.ApplyTo(StatsCollection);
            });
        }
    }       
}