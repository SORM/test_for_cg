﻿using Assets.Code.Services;
using Zenject;

namespace Assets.Code.Models
{
    public class PlayerModelFactory : IFactory<PlayerModel>
    {
        private readonly IPlayersCache _playersCache;

        public PlayerModelFactory(IPlayersCache playersCache)
        {
            _playersCache = playersCache;
        }
        public PlayerModel Create()
        {
            var p = new PlayerModel();
            _playersCache.Add(p);
            return p;
        }
    }
}