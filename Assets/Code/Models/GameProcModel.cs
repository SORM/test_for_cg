﻿using System.Linq;
using Assets.Code.Services;
using UniRx;
using UnityEngine;
using Zenject;

namespace Assets.Code.Models
{
    public class GameProcModel : IInitializable
    {
        public ReactiveCommand Play { get; } = new ReactiveCommand();
        public ReactiveCommand PlayWithBuffs { get; } = new ReactiveCommand();

        private readonly ISettingService _settingService;
        private readonly IPlayersCache _playersCache;

        private Data _cachedData;

        public GameProcModel(ISettingService settingService, IPlayersCache playersCache)
        {
            _settingService = settingService;
            _playersCache = playersCache;

            Play.Subscribe(unit => { PlayGame(false); });
            PlayWithBuffs.Subscribe(unit => { PlayGame(true); });
        }

        public void Initialize()
        {
            _settingService.SettingsAsColdObservable.Subscribe(data =>
            {
                _cachedData = data;
                PlayWithBuffs.Execute();
            });
        }

        private void PlayGame(bool useBuffs)
        {
            var p1 = _playersCache.PlayerModels[0];
            var p2 = _playersCache.PlayerModels[1];
            p1.Enemy.Value = p2;
            p2.Enemy.Value = p1;
            p1.Resurect.Execute(ExtractPlayGameData(_cachedData, useBuffs));
            p2.Resurect.Execute(ExtractPlayGameData(_cachedData, useBuffs));

            p1.Hp.Where(hp => hp <= 0).Subscribe(i => p2.Enemy.Value = null);
            p2.Hp.Where(hp => hp <= 0).Subscribe(i => p1.Enemy.Value = null);
        }

        private static PlayGameDataModel ExtractPlayGameData(Data data, bool useBuffs)
        {
            var statModels = data.stats?.Select(s => new StatModel().Init(s)).ToArray();
            var buffModels = data.buffs?.Select(b => new BuffModel().Init(b))
                .OrderBy(x => Random.value)
                .Take(Random.Range(data.settings.buffCountMin, data.settings.buffCountMax))
                .ToArray();
            var dataSettings = data.settings;

            var playGameData = new PlayGameDataModel(statModels, useBuffs ? buffModels : new BuffModel[0], dataSettings.buffCountMax, dataSettings.buffCountMin,
                dataSettings.allowDuplicateBuffs);
            return playGameData;
        }
    }
}