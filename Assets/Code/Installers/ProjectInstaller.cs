﻿using Assets.Code.Models;
using Assets.Code.Presenters;
using Assets.Code.Services;
using Zenject;

namespace Assets.Code.Installers
{
    public class ProjectInstaller : MonoInstaller<ProjectInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<SettingService>().AsSingle();

            Container.BindInterfacesTo<PlayersCache>().AsSingle();           
            Container.Bind<PlayerModel>().FromFactory<PlayerModelFactory>().AsTransient();
            
            Container.BindInterfacesAndSelfTo<GameProcModel>().AsSingle();           

            Container.Bind<ISpriteCache>().To<SpriteCache>().AsSingle();

            Container.Bind<StatModel>().AsTransient();   
            
            Container.BindInterfacesTo<CameraPresenter>().AsSingle().NonLazy();
        }
    }
}