﻿using Assets.Code.Models;
using Assets.Code.Presenters;
using UnityEngine;
using Zenject;

namespace Assets.Code.Installers
{
    public class ViewInstaller : MonoInstaller
    {
        [SerializeField]
        private GameProcPresenter _gameProcPresenter;
        [SerializeField]
        private GameObject _statPrefab;
        [SerializeField]
        private GameObject _buffPrefab;
        public override void InstallBindings()
        {
            Container.Bind<GameProcPresenter>().FromInstance(_gameProcPresenter).AsSingle();
            Container.Bind<IInitializable>().To<GameProcPresenter>().FromResolve();

            Container.BindFactory<StatModel, StatPresenter, StatPresenter.StatPresenterFactory>().FromComponentInNewPrefab(_statPrefab);
            Container.BindFactory<BuffModel, BuffPresenter, BuffPresenter.BuffPresenterFactory>().FromComponentInNewPrefab(_buffPrefab);
        }
    }
}