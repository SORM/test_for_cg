﻿using System;
using UniRx;
using UnityEngine;

namespace Assets.Code.Services
{
    class SettingService : ISettingService
    {
        public IReadOnlyReactiveProperty<Data> SettingsAsColdObservable
        {
            get
            {
                var json = Resources.Load<TextAsset>("data").text;
                var data = JsonUtility.FromJson<Data>(json);

                if (data == null)
                    throw new Exception("received data is empty");                

                return new ReadOnlyReactiveProperty<Data>(Observable.Return(data));
            }
        }
    }
}