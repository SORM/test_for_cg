﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.Services
{
    public class SpriteCache : ISpriteCache
    {
        private readonly Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();
        public Sprite Get(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            if (_sprites.ContainsKey(name))
                return _sprites[name];

            var s = Resources.Load<Sprite>($"Icons\\{name}");
            _sprites.Add(name, s);
            return s;
        }
    }
}