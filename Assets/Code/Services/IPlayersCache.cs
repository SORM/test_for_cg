﻿using Assets.Code.Models;
using UniRx;

namespace Assets.Code.Services
{
    public interface IPlayersCache
    {
        IReadOnlyReactiveCollection<PlayerModel> PlayerModels { get; }
        void Add(PlayerModel playerModel);
    }
}