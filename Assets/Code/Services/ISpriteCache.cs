﻿using UnityEngine;

namespace Assets.Code.Services
{
    public interface ISpriteCache
    {
        Sprite Get(string name);
    }
}