﻿using UniRx;

namespace Assets.Code.Services
{
    public interface ISettingService
    {
        IReadOnlyReactiveProperty<Data> SettingsAsColdObservable { get; }
    }
}