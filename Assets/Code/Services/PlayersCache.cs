﻿using System.Collections.Generic;
using Assets.Code.Models;
using UniRx;

namespace Assets.Code.Services
{
    public class PlayersCache : IPlayersCache
    {
        public IReadOnlyReactiveCollection<PlayerModel> PlayerModels => _models;

        private readonly ReactiveCollection<PlayerModel> _models = new ReactiveCollection<PlayerModel>(new List<PlayerModel>());

        public void Add(PlayerModel playerModel) => _models.Add(playerModel);
    }
}